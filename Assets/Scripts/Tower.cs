﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    //[SerializeField] GameObject _AreaAOE;
    //[SerializeField] float _radiusAOE = 1.5f;

    Monster _target;
    LineRenderer _lineRenderer;

    float _lastAttack;
    float _attackCooldown = 0.66f;
    float _damage = 1f;

    // Colliders
    [SerializeField] GameObject _circleCollider;
    [SerializeField] GameObject _coneCollider;
    [SerializeField] GameObject _trapCollider;

    [SerializeField] GameObject _bodyTop;

    public void Initialize(TowerType type)
    {
        Debug.Log("[Tower] Initialize : " + type);
        _lineRenderer = GetComponent<LineRenderer>();

        //Collider Logic
        _circleCollider.SetActive(type == TowerType.Circle);
        _coneCollider.SetActive(type == TowerType.Cone);
        _trapCollider.SetActive(type == TowerType.Trap);
    }

    void OnTriggerEnter(Collider thisCollider)
    {
        TestTarget(thisCollider);
    }

    // Start is called before the first frame update
    void OnTriggerStay(Collider thisCollider)
    {
        TestTarget(thisCollider);
    }

    void TestTarget(Collider thisCollider)
    {
        Monster m = thisCollider.GetComponentInParent<Monster>();
        if (m != null && m != _target)
            Target(m);
    }

    void Target(Monster monster)
    {
        _target = monster;
        _lineRenderer.positionCount = 2;
    }

    void Update()
    {
        _lineRenderer.enabled = (_target != null);
        if (_target == null)
            return;

        //Visual
        _lineRenderer.SetPosition(0, gameObject.transform.position + Vector3.up * 0.45f);
        _lineRenderer.SetPosition(1, _target.transform.position + Vector3.up * 0.3f);

        float deltaAngle = Vector3.Angle(transform.forward, (_target.transform.position - gameObject.transform.position));
        _bodyTop.transform.localRotation = Quaternion.Euler(new Vector3(-90f, deltaAngle, 0f));
        //_bodyTop.transform.LookAt(_target.transform.position, Vector3.right);
        //_bodyTop.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
        //_bodyTop.transform.Rotate(Vector3.forward, deltaAngle);            


        //Damage
        if (Time.time - _lastAttack >= _attackCooldown)
        {
            _lastAttack = Time.time;
            _target.AddHP(-_damage);
        }
    }

    /*void LateUpdate()
    {
        transform.parent.localPosition.Set(transform.localPosition.x, -transform.parent.position.y, transform.localPosition.z);
    }*/
}

public enum TowerType
{
    Circle,
    Cone,
    Trap
}
