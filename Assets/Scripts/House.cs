﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    [System.NonSerialized] public float MaxHP = 10f;
    public float CurHP;

    [SerializeField] UnityEngine.UI.Slider _uiSlider;

    public void Initialize()
    {
        SetHP(MaxHP);
    }

    public void AddHP(float value)
    {
        SetHP(CurHP + value);
    }

    void SetHP(float hp)
    {
        CurHP = hp;

        if(_uiSlider != null)
            _uiSlider.value = CurHP / MaxHP;

        if (CurHP <= 0f)
            LevelManager.Instance.Lose();
    }
}
