﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI.text;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;

    public Tower TowerPrefab;
    public House HouseReference;
    [SerializeField] private List<Level> _levels;
    int _curLevelIndex = 0;
    [System.NonSerialized] public Level CurLevel;  

    bool _isWaitingOnTower;
    bool _debugSkipTower = false;

    List<Tower> _towers = new List<Tower>();

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Initialize();
    }

    // Start is called before the first frame update
    void Initialize()
    {
        _isWaitingOnTower = true;
        UIManager.Instance.SetRoofText("Position Turret to Start!");
    }

    void Update()
    {
        if(_isWaitingOnTower && ( _towers.Count > 0 || _debugSkipTower))
        {
            _isWaitingOnTower = false;
            SpawnLevel(_levels[_curLevelIndex]);
        }       
    }

    void SpawnLevel(Level level)
    {
        UIManager.Instance.SetRoofText("Starting Level " + (_curLevelIndex + 1));
        
        LeanTween.delayedCall(1f, () =>
        {
            Level CurLevel = Instantiate(level);
            HouseGroup hg = FindObjectOfType<HouseGroup>();
            CurLevel.transform.SetParent(hg.transform);
            CurLevel.gameObject.SetActive(true);
            CurLevel.Initialize();
        });
        HouseReference.Initialize();
    }

    public TowerType RegisterTurret(Tower turret, bool isRegistering = true)
    {
        if (isRegistering)
            _towers.Add(turret);
        else
            _towers.Remove(turret);

        if(_curLevelIndex == 0)
            return TowerType.Circle;
        else if (_curLevelIndex == 1)
            return TowerType.Cone;
        else 
            return TowerType.Circle;
    }

    public void EndLevel()
    {
        CurLevel.gameObject.SetActive(false);
        UIManager.Instance.SetRoofText("Completed Level " + (_curLevelIndex + 1) + "!!");
        LeanTween.delayedCall(1f, () =>
        {
            _curLevelIndex++;
            if (_curLevelIndex < _levels.Count)
            {
                SpawnLevel(_levels[_curLevelIndex]);
            }
            else
            {
                UIManager.Instance.SetRoofText("Game Finish! " + (_curLevelIndex + 1) + "!!");
            }
        });
    }

    public void Lose()
    {
        UIManager.Instance.SetRoofText("You Lose! House Destroyed!");
    }
}
