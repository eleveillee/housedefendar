﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

using Random = UnityEngine.Random;

public static class ExtensionHelper {

    #region BASE
    // bool
    public static float GetSign(this bool b)
    {
        return b ? 1f : -1f;
    }
    #endregion

    #region ACTIONS
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
            action.Invoke();
    }
    
    public static void SafeInvoke<T1>(this Action<T1> action, T1 t1)
    {
        if (action != null)
            action.Invoke(t1);
    }

    public static void SafeInvoke<T1, T2>(this Action<T1, T2> action, T1 t1, T2 t2)
    {
        if (action != null)
            action.Invoke(t1, t2);
    }

    public static void SafeInvoke<T1, T2, T3>(this Action<T1, T2, T3> action, T1 t1, T2 t2, T3 t3)
    {
        if (action != null)
            action.Invoke(t1, t2, t3);
    }
    #endregion

    #region GEOMETRY
    // Rect
    public static Vector2 GetRandom(this Rect rect, Vector2 offset)
    {
        // offset is increase in size (1f = +1f to all side)

        float posX = Random.Range(rect.xMin - offset.x, rect.xMax + offset.x);
        float posY = Random.Range(rect.yMin - offset.y, rect.yMax + offset.y);

        return new Vector2(posX, posY);
    }

    // Vector2
    public static float GetRandomValue(this Vector2 vector)
    {
        return Random.Range(vector.x, vector.y);
    }
    
    public static float Sum(this Vector2 vector)
    {
        return vector.x + vector.y;
    }

    public static float Sub(this Vector2 vector)
    {
        return vector.y - vector.x;
    }

    public static bool Contains(this Vector2 vector, float value)
    {
        return vector.x >= value && value <= vector.y;
    }
    

    // Vector3
    public static float AngleWith(this Vector3 vectorFrom, Vector3 vectorTo)
    {
        Vector3 diff =  vectorTo - vectorFrom;
        diff.z = 0f;
        diff.Normalize();

        return Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
    }
    #endregion

    #region TOOLS
    // List
    public static T GetRandomValue<T>(this List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    public static T GetRandomValueExcept<T>(this List<T> list, T exception)
    {
        List<T> newList = list.ToList();
        newList.Remove(exception);
        return newList.GetRandomValue();

        /*T newT = list.GetRandomValue();
        while (newT == exception)
        {
            newT = list.GetRandomValue();
        }*/
    }

    

    public static void MoveItemAtIndexToFront<T>(this List<T> list, int index)
    {
        T item = list[index];
        for (int i = index; i > 0; i--)
            list[i] = list[i - 1];
        list[0] = item;
    }

    public static void MoveItemAtIndexToEnd<T>(this List<T> list, int index)
    {
        T t = list[index];
        list.RemoveAt(index);
        list.Add(t);
    }

    public static bool IsNullOrEmpty<T>(this List<T> list)
    {
        return (list == null || list.Count <= 0);
    }
    #endregion

    #region UNITYENGINE
    // Transform
    public static void SetX(this Transform transform, float value) { transform.localPosition = new Vector3(value, transform.localPosition.y, transform.localPosition.z); }
    public static void SetY(this Transform transform, float value) { transform.localPosition = new Vector3(transform.localPosition.x, value, transform.localPosition.z); }
    public static void AddY(this Transform transform, float value) { transform.SetY(transform.localPosition.y + value); }
    public static void SetZ(this Transform transform, float value) { transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, value); }
    
    // GameObject
    public static void ToggleActive(this GameObject go)
    {
        go.SetActive(!go.activeSelf);
    }

    public static void SetLayerAllChilds(this Transform trans, int layer)
    {
        trans.gameObject.layer = layer;
        foreach (Transform child in trans)
            child.SetLayerAllChilds(layer);
    }
    // Collider2D
    public static bool IsTarget(this Collider2D collider2D, Lean.LeanFinger finger)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(finger.ScreenPosition);
        Vector2 touchPos = new Vector2(wp.x, wp.y);

        if (collider2D == null)
            return false;

        return collider2D.bounds.Contains(touchPos);
    }
    
    // Text
    public static void SetValue(this UnityEngine.UI.Text text, float value, float duration = 0.0f)
    {
        text.text = String.Format("{0:0}", value);
    }
    #endregion

}