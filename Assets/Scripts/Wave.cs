﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Wave : MonoBehaviour
{
    public List<Monster> Monsters;
    Level _parentLevel;

    // Start is called before the first frame update
    public void Initialize(Level level)
    {
        //Debug.Log("[Wave] " + gameObject.name + " Initialize");
        _parentLevel = level;
        gameObject.SetActive(true);
        Monsters = GetComponentsInChildren<Monster>(true).ToList();
        foreach(Monster m in Monsters)
        {
            m.Initialize(this);
        }
    }

    // Update is called once per frame
    public void RegisterMonsterDeath(Monster monster)
    {
        Monsters.Remove(monster);
        if (Monsters.Count <= 0)
            EndWave();
    }

    void EndWave()
    {
        _parentLevel.WaveEnded();
        gameObject.SetActive(false);
    }
}
