﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    House _house;
    Wave _wave;
    [SerializeField] UnityEngine.UI.Slider _uiSlider;

    [SerializeField] Animator _animator;

    // Stats
    [System.NonSerialized] public float MaxHP = 3f;
    public float CurHP;
    float _speed = 1.25f;
    float _minRange = 2f;
    float _damage = 1f;


    bool _isAttacking;
    float _lastAttack;
    float _attackCooldown = 0.66f;

    public System.Action<Monster> OnDestroyCallback;
    // Start is called before the first frame update
    public void Initialize(Wave wave)
    {
        _wave = wave;
        _house = LevelManager.Instance.HouseReference;
        gameObject.SetActive(true);
        SetHP(MaxHP);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf && _house != null && _house.gameObject.activeSelf)
        {

            if (Vector3.Distance(transform.position, _house.transform.position) < _minRange)
                Attack();
            else
                Move();
        }

        _uiSlider.transform.LookAt(Camera.main.transform, Vector3.up);
    }

    void Attack()
    {
        if(!_isAttacking)
        {
            GetComponentInChildren<MechShoot>().enabled = true;
            _animator.SetTrigger("Attack");
            _isAttacking = true;
        }

        //Damage
        if (Time.time - _lastAttack >= _attackCooldown)
        {
            _lastAttack = Time.time;
            Debug.Log("[Monster] Attack");
            _house.AddHP(-_damage);
        }
    }

    void Move()
    {
        Debug.Log("[Monster] Move");
        Vector3 delta = (_house.transform.position - transform.position).normalized * _speed * 0.01f;
        transform.position += delta;
    }

    void LateUpdate()
    {
        transform.LookAt(_house.transform.position, Vector3.up);
    }

    public void AddHP(float value)
    {
        SetHP(CurHP + value);
    }

    void SetHP(float hp)
    {
        CurHP = hp;

        _uiSlider.value = CurHP / MaxHP;

        if(CurHP <= 0f)
            Die();
    }

    public void Die()
    {
        _wave.RegisterMonsterDeath(this);

        GetComponentInChildren<MechShoot>().enabled = false;
        GetComponentInChildren<MechHit>().enabled = true;
        
        LeanTween.delayedCall(0.5f, () => Destroy(gameObject));
    }

    void OnDestroy()
    {
        OnDestroyCallback.SafeInvoke(this);
    }
}
