﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseGroup : MonoBehaviour
{
    bool _isFirstTime = true;

    void Start()
    {
        Debug.Log("GroupAwake");
        CustomTrackableEventHandler cteh = GetComponentInParent<CustomTrackableEventHandler>();
        cteh.OnTrackingFound += OnTrackingFound;
        gameObject.SetActive(false);
        //CustomTrackableEventHandler _cteh.OnTrackingLost += OnTrackingLost;
    }


    // Start is called before the first frame update
    void OnTrackingFound()
    {
        Debug.Log("GroupTracking!!!!!!!!!!!!!!!!!!");
        _isFirstTime = false;
        LeanTween.delayedCall(1.5f, () =>
        {
            UIManager.Instance.DisableCorners();
            Animator animator = GetComponent<Animator>();
            animator.enabled = true;
            animator.SetTrigger("HouseSpawn");
            Debug.Log("HouseSpawn");
        });
    }
}
