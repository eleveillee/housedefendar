﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Level : MonoBehaviour
{
    bool _isInitialized;
    List<Wave> _waves;
    int _curWaveIndex;
    public Wave CurWave;

    // Start is called before the first frame update
    /*void OnEnable()
    {
        Initialize();
    }*/

    void Awake()
    {
        //_waves = GetComponentsInChildren<Wave>(true).ToList();
        //_waves.ForEach(w => w.gameObject.SetActive(false));
    }

    public void Initialize()
    {
        _waves = GetComponentsInChildren<Wave>(true).ToList();
        SpawnWave(_waves[_curWaveIndex]);
    }

    // Update is called once per frame
    public void WaveEnded()
    {
        Debug.Log("[Level] WaveEnded");
        _curWaveIndex++;
        if(_curWaveIndex < _waves.Count)
        {
            SpawnWave(_waves[_curWaveIndex]);
        }
        else
        {
            End();
        }
    }

    void SpawnWave(Wave wave)
    {
        UIManager.Instance.SetRoofText("Wave " + (_curWaveIndex + 1));
        CurWave = wave;
        wave.Initialize(this);
    }

    void End()
    {
        LevelManager.Instance.EndLevel();
    }
}
