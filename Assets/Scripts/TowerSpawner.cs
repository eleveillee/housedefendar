﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerSpawner : MonoBehaviour
{
    Tower _tower;
    CustomTrackableEventHandler _cteh;
    void Awake()
    {
        _cteh = GetComponent<CustomTrackableEventHandler>();
        _cteh.OnTrackingFound += OnTrackingFound;
    }

    // Start is called before the first frame update
    void OnTrackingFound()
    {
        if (_tower == null && _cteh.IsSpawningTower)
            SpawnTower();

        //else
            //_tower.gameObject.SetActive(true);
    }

    void SpawnTower()
    {
        _tower = Instantiate(LevelManager.Instance.TowerPrefab);
        _tower.transform.parent = transform;
        _tower.transform.localPosition = new Vector3(0f, 0f, 0f);
        _tower.transform.localRotation = Quaternion.identity;

        TowerType type = LevelManager.Instance.RegisterTurret(_tower);
        _tower.Initialize(type);
    }

    // Update is called once per frame
    void OnTrackingLost()
    {
        LevelManager.Instance.RegisterTurret(_tower, false);
        //_tower.gameObject.SetActive(false);
    }
}
