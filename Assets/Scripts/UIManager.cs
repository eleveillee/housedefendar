﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [SerializeField] Camera _ARCamera;
    [SerializeField] Marker _markerPrefab;
    [SerializeField] Vector2 _uiSize = new Vector2();
    //List<Image> _markers = new List<Image>();
    Dictionary<Monster, Marker> _monsterMarkers = new Dictionary<Monster, Marker>();

    [SerializeField] private TMPro.TextMeshPro _textRoof;
    [SerializeField] GameObject _cornerGO;

    // DEBUGS
    public Vector3 _monsterDirection;
    public Vector2 _markerLocalPos;
    bool _isUsingMarker = true; // NotWorking for now

    // Start is called before the first frame update
    void Awake()
    {
        Instance = this;
        _cornerGO.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (!_isUsingMarker || LevelManager.Instance.CurLevel == null || LevelManager.Instance.CurLevel.CurWave == null || LevelManager.Instance.CurLevel.CurWave.Monsters == null)
            return;

        //Debug.Log("Update : " + LevelManager.Instance.CurLevel.CurWave.Monsters.Count);
        foreach(Monster monster in LevelManager.Instance.CurLevel.CurWave.Monsters)
        {
            if(monster == null)
            {
                RemoveMarker(monster);
                continue;
            }
            Marker marker;

            if (!_monsterMarkers.ContainsKey(monster))
            {
                marker = Instantiate(_markerPrefab);
                marker.transform.SetParent(transform);
                marker.transform.localPosition = new Vector2(-_uiSize.x, 0f);
                _monsterMarkers.Add(monster, marker);
                monster.OnDestroyCallback += OnDestroyedMonster;
            }
            else
            {
                _monsterDirection = (monster.transform.position - _ARCamera.transform.forward).normalized;
                Vector3 camPos = Camera.main.WorldToViewportPoint(monster.transform.position);
                if(camPos.x > 0f && camPos.x < 1f && camPos.y > 0f && camPos.y < 1f)
                {
                    _monsterMarkers[monster].gameObject.SetActive(false);
                }
                else
                {
                    _monsterMarkers[monster].gameObject.SetActive(true);
                    float markerLocalPosX = 0f;
                    if      (camPos.x < 0f) markerLocalPosX = -_uiSize.x;
                    else if (camPos.x > 1f) markerLocalPosX = _uiSize.x;
                    else                    markerLocalPosX = (-_uiSize.x / 2f) + _uiSize.x * camPos.x;

                    float markerLocalPosY = 0f;
                    if      (camPos.y < 0f) markerLocalPosY = -_uiSize.y;
                    else if (camPos.y > 1f) markerLocalPosY = _uiSize.y;
                    else                    markerLocalPosY = (-_uiSize.y / 2f) + _uiSize.y * camPos.y;

                    _markerLocalPos = new Vector2(markerLocalPosX, markerLocalPosY);
                    _monsterMarkers[monster].transform.localPosition = _markerLocalPos;
                }
            }

        }
    }

    void OnDestroyedMonster(Monster monster)
    {
        RemoveMarker(monster);
    }

    void RemoveMarker(Monster monster)
    {
        if (_monsterMarkers.ContainsKey(monster))
        {
            Destroy(_monsterMarkers[monster].gameObject);
            _monsterMarkers.Remove(monster);
        }
    }

    public void SetRoofText(string newText)
    {
        _textRoof.text = newText;
    }

    public void DisableCorners()
    {
        _cornerGO.gameObject.SetActive(false);
    }

}
